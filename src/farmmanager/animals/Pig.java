package farmmanager.animals;

public class Pig extends Animal {
    private final int MAX_VALUE = 700;

    public Pig() {
        value = 400;
    }

    @Override
    public void feed() {
        if (value < MAX_VALUE) {
            value = value + value / 5;
        }

    }

    @Override
    public int costToFeed() {
        return 40;
    }

    @Override
    public String getType() {
        return "Pig";
    }

    @Override
    public String toString() {
        return getType() + " - $" + value;
    }
}
