Challenge 02
==========
The skeleton code is found in the `
farmmanager` package.

### Introduction
In this game, you can buy animals, raise them, and sell them back for a profit. Just remember don’t run out of money!

Currently, the game has the following commands:
* `buy <animal name>` - buys new animals.
* `sell` - sells all your animals.
* `feed` - feeds as many animals as you have money for.
* `feed <animal type>` - feeds as many animals as you have money for that are of the given type.
* `stock` - lists which animals you have in stock.
* `money` - tells you how much money you have.
* `harvest` – harvests all animals that produce products.
* `quit` – provide a summary financial statement then exit the game.

Note that some commands will not work properly until you have implemented the required functionality.

### Task One. Complete Farm Constructor
Complete the constructor of the **Farm** class which takes an integer parameter in the Farm class so that:

 1. **money** is initialised to the given parameter.
 2. **STARTING_MONEY** is initialised to the given parameter.
 3.  **animals** array is initialised to hold 10 **Animal** objects.

### Task Two. Complete Farm’s buyAnimal method
The **buyAnimal** method in the **Farm** class allows users to buy animals for the farm. The purchase of an animal is only successful when:
 1. The animal type exists,
 2. The money is sufficient, and
 3. The animals array is not full, i.e. the array does not contain any null elements.

The first two conditions have been implemented in the **buyAnimal** method. Now, complete the method by implementing the third condition. Here is some pseduocode to help you to complete this method:

* Go through each element in the **animals** array
    * If the element is null, assign the newly created animal to that element. Deduct the price of the animal from the money you have for the farm. Then, return true.


### Task Three. Complete Farm’s feed method
Complete the **feed** method in the **Farm** class to feed all the animals of the specified type on the farm. An animal is only fed when there is enough money to feed it. When you fed an animal, you also need to subtract the cost to feed from the money you have on the farm, and call the feed method on the animal. Don’t forget to stop going through the **animals** array when there is no more animal to feed (i.e. when the animal becomes null)! 
<pre>Hint: You may use the <b>getType</b> method on the animal to get the animal type. Then, compare to see if the type of animal matches the one specified from the parameter. The match should be case insensitive.</pre>

### Task Four. Complete Farm’s printStock method
Complete the **printStock** method in the **Farm** class to print information for all animals on the farm by calling the **toString** method on each animal. If there are no animals on the farm, simply print the message "No animals on the farm!". 

 <pre>Hint: There are no animals on the farm if all elements in the <b>animals</b> array are null.</pre>

After completing these tasks, the **FarmManager** game should produce similar outputs as the following example: 

![](FarmManagerCapture1.PNG)

### Task Five. Create New Animals



*   Create a **Chicken** class, which extends the **Animal** class.
    1. Create a private constant **int** field, which stores 300. Name the field **MAX_VALULE**.
    2. Create a constructor for the **Chicken** class that does not take any parameters. Within the constructor, set the **value** for the chicken to 200.
    3. Implement the **feed** method so that the **value** for the chicken increases when the chicken has been fed. The formula for increasing the **value** is:    
        `value = value + (MAX_VALUE - value) / 2;`
	   Note that the **value** of the chicken should not exceed the **MAX_VALUE**.
    4. Implement the **costToFeed** method to return 3 as the cost for feeding each chicken.
    5. Implement the **getType** method to return "Chicken".
    6. Implement the **toString** method so that the user knows what it is. The String should be similar to the one in the **Cow** class.

*   Create a unique  type of animal, which also extends the **Animal** class. Implement the appropriate constructor and methods for your animal. Don’t forget to also implement an appropriate **toString** method on your animal.

*   Modify the **getAnimal** method in the **Farm** class appropriately to include **Chicken** and the animal type that you created in b).

### Task Six. Collect Products from Animals
Now, we would like to make some money by harvesting products from our animals. We want to be able to milk cows and collect eggs from chickens. In order to do so, you need to do the following steps:

 1. Modify the **Cow** class to implement the **IProductionAnimal** interface. A cow can only be milked if its value has reached the maximum. The money you can make from milking a harvestable cow is $20, otherwise $0.

 2. Modify the **Chicken** class to implement the **IProductionAnimal** interface. You can always collect eggs from a chicken regardless of its value. The money you can make from collecting eggs is $5.

 3. Complete the **harvestAll** method in the **Farm** class so that you can make money by harvesting the products from all animals that implement the **IProductionAnimal** interface. You can only harvest an animal if it is harvestable. If the animal is harvestable, use the **harvest** method on the animal and add the money made from harvesting to the **money** you have on the farm.

    <pre>Hint: use instanceof to determine if an animal is an instance of <b>IProductionAnimal</b>.</pre>

After completing all tasks, the **FarmManager** game should produce similar outputs as the following example:

![](FarmManagerCapture2.PNG)